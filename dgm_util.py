import os, sys, traceback, time

def printstacktrace():
    """
    prints the stack trace
    """
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traceback.print_exception(exc_type, exc_value, exc_traceback, file=sys.stdout)
    
def now():
    """
    returns current timestamp
    """
    return time.time()
    
       
def config_gre(bridge, localip, remoteip):
    """
    Configures GRE link

    bridge -- string bridge name
    localip -- local string ip
    remoteip -- remote string ip
    
    Example:
    ip link add tun0 type gretap remote 10.0.65.1 local 10.0.65.3 ttl 255
    brctl addif br0 tun0
    ip link set tun0 up
    """
    tun_name = "gre" + remoteip
    cmd1 = "ip link add " + tun_name + " type gretap remote " + remoteip + " local " + localip + " ttl 255"
    cmd2 = "brctl addif " + bridge + " " + tun_name
    cmd3 = "ip link set " + tun_name + " up"
    os.system('sudo sh -c "' + cmd1 + '"')
    os.system('sudo sh -c "' + cmd2 + '"')
    os.system('sudo sh -c "' + cmd3 + '"')
    return



